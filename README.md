# Activity Tracker #
The `ActivityTracker` app uses the [pynput](https://pynput.readthedocs.io/en/latest/) library to register keyboard and mouse events in the background and for each event it sends a marker to the local network using [LSL](https://github.com/sccn/labstreaminglayer). The following event markers are produced:

* keyboard event: 1
* mouse event: 2
* key released (mouse or keyboard): 3
* F10 key pressed: 4

**Note:** The app doesn't track the content of user's activity, only whether **any** keyboard or mouse buttons is pressed.


# Installation instructions #


## Linux ##
1. Install a Python 3.x.
2. Open a terminal.
3. `cd` into this folder with `cd /path/to/this/folder`
4. Install dependency with `pip3 install -r requirements.txt`
4. Run the app with `python3 ActivityTracker.py`

## Mac OSX ##
1. Install a Python 3.x (Comes with Anaconda or PyCharm)
2. Clone or download folder from Bitbucket repository.
3. `cd` into the folder of the script in `/App`.
4. Install dependecies.(Normally just `pip install pynput` and `pip install pylsl`. If there is any other dependey requirement, just install the specific requirement in the error message.
5. MUST run the app in root with `sudo`!!!
   Run the app with `sudo python AcitivityMonitor.py`
6. Your recording session will be saved in the folder `/result` with an automatically generated name.
7. Leave the program running in the backgroud. Press `Esc` to stop tracking and save the data.

## Windows ##
1. Install a Python 3.x (Comes with Anaconda or PyCharm)
2. Clone or download folder from Bitbucket repository.
3. `cd` into the folder of the script in `/App`.
4. Install dependecies.(Normally just `pip install pynput` and `pip install pylsl`. If there is any other dependey requirement, just install the specific requirement in the error message.
5. The program may encounter some admin error. If so, give full access to the folder under your current windows account.
6. Run the app with `python AcitivityMonitor.py`
7. Leave the program running in the backgroud. Press `Esc` to stop tracking and save the data.