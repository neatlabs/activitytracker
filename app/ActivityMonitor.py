from pylsl import StreamInfo,StreamInlet, resolve_byprop, StreamOutlet
from time import time
import csv
import uuid
import os
from pynput import keyboard, mouse
from threading import Thread
stop = False

class ActivityTracker:

    def __init__(self):

        self.info = StreamInfo("KeyLogger", 'Markers', 1, 0, 'float32')
        self.outlet = StreamOutlet(self.info)
        self.value = None

    def on_press(self,key):
        self.value = 3 if key == keyboard.Key.f10 else 1
        self.outlet.push_sample([self.value])

    def on_release(self,key):
        self.value = 0.001
        self.outlet.push_sample([self.value])
        if key == keyboard.Key.esc:
            global stop
            stop = True
            return False

    def on_click(self,x, y, button, pressed):
        self.value = 2 if pressed else 0.001
        self.outlet.push_sample([self.value])
        global stop
        if stop == True:
            return False


    def on_scroll(self,x, y, dx, dy):
        self.value = 2
        self.outlet.push_sample([self.value])


class ActivityCapture():

    def __init__(self, fileToSave, verbose=False):
        self.verbose = verbose
        resultDir = os.path.join(".." ,"result")
        if not os.path.exists(resultDir):
            os.mkdir(resultDir)
        self.filename = os.path.join(resultDir,fileToSave+".csv")
        self.csvfile = open(self.filename,'w', newline='')
        self.writer = csv.DictWriter(self.csvfile, fieldnames=['data', 'time_stamp'])
        self.writer.writeheader()

    def startCapturing(self):


        results = resolve_byprop(prop='name', value="KeyLogger")

        while len(results) == 0:
            time.sleep(0.25)
        info = results[0]
        inlet = StreamInlet(info, recover=False)

        # Read data in forever
        global stop
        try:
            while stop==False:
                data, timeStamp = inlet.pull_sample()
                if data:
                    self.writer.writerow({'data': data[0],'time_stamp': timeStamp})
                    if self.verbose:
                        print("writing")
                        print(data, timeStamp)
            print("done")
            self.csvfile.close()
            return

        except Exception as e:
            print(e)
            pass



verbose = False
filename = str(uuid.uuid4())  # input("Enter a file name: ")

actTracker = ActivityTracker()
actCapture = ActivityCapture(filename, verbose)

kListener = keyboard.Listener(on_press=actTracker.on_press, on_release=actTracker.on_release)
mListener = mouse.Listener(on_click=actTracker.on_click)
thread = Thread(target=actCapture.startCapturing,args=())
kListener.start()
mListener.start()
thread.start()

kListener.join()
mListener.join()
thread.join()









